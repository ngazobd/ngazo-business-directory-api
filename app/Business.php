<?php

namespace App;

use App\Events\BusinessPublished;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    use RecordsActivity;

    protected $fillable = ['brand_name', 'user_id', 'active'];

    protected $events = [
        'created' => BusinessPublished::class
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'business_likes', 'business_id', 'user_id');
    }

    public function ratings()
    {
        return $this->belongsToMany(User::class, 'business_ratings', 'business_id', 'user_id')->withPivot(['value']);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function tags()
    {
        return $this->belongsToMany(SubCategory::class, 'business_sub_categories');
    }

    public function path()
    {
        return '/businesses/' . $this->id;
    }

    public function addComment($comment)
    {
        $this->comments()->create($comment);
    }
}
