<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessRating extends Model
{
    protected $fillable = ['user_id', 'business_id', 'value'];

    public $incrementing = false;
}
