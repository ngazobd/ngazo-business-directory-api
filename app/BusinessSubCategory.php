<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessSubCategory extends Model
{
    public $incrementing = false;
}
