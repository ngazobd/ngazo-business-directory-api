<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image'
    ];

    protected $hidden = [
        'image',
        'created_at',
        'updated_at'
    ];

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class);
    }

}
