<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use RecordsActivity;

    protected $fillable = ['user_id', 'business_id', 'body'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
