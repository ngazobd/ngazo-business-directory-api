<?php

namespace App\Http\Controllers\API\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivitiesController extends Controller
{
    public function show($username)
    {
        $user = User::whereUsername($username)->firstOrFail();

       	$activity = $user->getActivity();

       	return view('api.v1.activity.show', compact('activity'));
    }
}
