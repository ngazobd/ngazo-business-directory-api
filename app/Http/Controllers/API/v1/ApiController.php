<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function index()
    {
    	$collection = collect([
    		'users_url' => Config('app.url') . '/api/v1/users',
    		'user_url' => Config('app.url') . '/api/v1/users/{user}',
    		'businesses_url' => Config('app.url') . '/api/v1/businesses',
    		'business_url' => Config('app.url') . '/api/v1/businesses/{business}',
    		'categories_url' => Config('app.url') . '/api/v1/categories',
    		'category_url' => Config('app.url') . '/api/v1/categories/{category}',
    		'sub_categories_url' => Config('app.url') . '/api/v1/sub-categories',
    		'sub_category_url' => Config('app.url') . '/api/v1/sub-categories/{sub_category}',
    		'addresses_url' => Config('app.url') . '/api/v1/addresses',
    		'address_url' => Config('app.url') . '/api/v1/addresses/{address}',
    		'comments_url' => Config('app.url') . '/api/v1/comments',
            'comment_url' => Config('app.url') . '/api/v1/comments/{comment}',
            'user_notifications_url' => Config('app.url') . '/api/v1/users/{user}/notifications',
    		'user_unread_notifications_url' => Config('app.url') . '/api/v1/users/{user}/notifications?status=unread',
    	]);
    	return $collection;
    }
}
