<?php

namespace App\Http\Controllers\API\v1;

use App\Business;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class BusinessesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = Business::withCount(['addresses', 'likes', 'ratings', 'comments'])->latest()->get();

        return $businesses;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (request('brand_name') == null || request('user_id') == null) {
            return Response::json([
                'error' => [
                    'message' => 'Insufficient form fields.'
                ]
            ]);
        }

        $userId = request('user_id');
        $user = User::find($userId);
        if ($user == null) {
            return Response::json([
                'message' => 'Not Found',
                'documentation_url' => Config('app.url') . '/api/v1/docs'
            ]);
        }

        $business = new Business();
        $business->brand_name = request('brand_name');
        $business->user_id = request('user_id');
        $business->website = request('website');
        $business->description = request('description');
        $business->tin = request('tin');
        $business->save();

        return Response::json([
            'created' => true,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business = Business::withCount('likes', 'ratings')->with(['addresses', 'comments', 'galleries'])->find($id);

        if (!$business) {
            return Response::json([
                'error' => [
                    'message' => 'Business not found.',
                    'status' => 'FAIL'
                ]
            ], 404);
        }

        return $business;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $business = Business::findOrFail($id);

        $business->brand_name = request('brand_name');
        $business->website = request('website');
        $business->description = request('description');
        $business->logo = request('logo');
        $business->cover_photo = request('cover_photo');
        $business->tin = request('tin');
        $business->save();

        return Response::json([
            'updated' => true,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $business = Business::find($id);

        if ($business == null) {
            return Response::json([
                'error' => [
                    'message' => 'Business not found',
                    'status' => 'Fail'
                ]
            ]);
        }

        $business->active = false;
        $business->save();

        return Response::json([
            'deleted' => true
        ]);
    }
}
