<?php

namespace App\Http\Controllers\API\v1;

use App\User;
use App\Business;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class LikesController extends Controller
{
    public function store(Business $business)
    {
        $user = User::find(request('user_id'));

        $business->likes()->attach($user->id);

        return Response::json([
            'liked' => true,
        ]);
    }

    public function destroy(Business $business)
    {
        $user = User::find(request('user_id'));

        $business->likes()->detach($user->id);

        return Response::json([
            'unliked' => true,
        ]);
    }
}
