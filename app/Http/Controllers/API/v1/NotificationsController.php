<?php

namespace App\Http\Controllers\API\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class NotificationsController extends Controller
{
    public function index(User $user)
    {
    	if (request()->has('status')) {
    		if (request('status') === 'unread') {
    			return $user->unreadNotifications;
    		}
    	}
    	return $user->notifications;
    }

    public function destroy(User $user)
    {
    	$user->unreadNotifications->map(function ($n) {
    		$n->markAsRead();
    	});

    	return Response::json([
    		'read' => true
    	]);
    }
}
