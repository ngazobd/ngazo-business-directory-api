<?php

namespace App\Http\Controllers\API\v1;

use App\Business;
use App\BusinessRating;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class RatingsController extends Controller
{
    public function store(Business $business)
    {
        $user = User::findOrFail(request('user_id'));

        BusinessRating::create([
            'user_id' => $user->id,
            'business_id' => $business->id,
            'value' => request('value'),
        ]);

        return Response::json([
            'rated' => true,
        ]);
    }

}
