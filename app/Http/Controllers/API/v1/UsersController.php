<?php

namespace App\Http\Controllers\API\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::latest()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->phone = request('phone');
        $user->password = bcrypt(request('password'));
        $user->name = request('name');
        $user->email = request('email');
        $user->avatar = request('avatar');
        $user->gender = request('gender');
        $user->account_type = request('account_type');
        $user->device = request('device');
        $user->save();

        if (!$user->exists) {
            return Response::json([
                'created' => false,
            ]);
        }

        return Response::json([
            'created' => true,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->phone = request('phone');
        $user->password = bcrypt(request('password'));
        $user->name = request('name');
        $user->avatar = request('avatar');
        $user->gender = request('gender');
        $user->account_type = request('account_type');
        $user->device = request('device');
        $user->save();

        return Response::json([
            'updated' => true,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return Response::json([
            'deleted' => true
        ]);
    }
}
