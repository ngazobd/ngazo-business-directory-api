<?php

namespace App\Http\Controllers;

use App\Address;
use App\Business;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Business $business)
    {
        return view('addresses.create', compact('business'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Business $business, Request $request)
    {
        $this->validate($request, [
            'street' => 'required'
        ]);

        $address = new Address();
        $address->country = request('country');
        $address->street = request('street');
        $address->phone1 = request('phone1');
        $address->phone2 = request('phone2');
        $address->phone3 = request('phone3');
        $address->email1 = request('email1');
        $address->email2 = request('email2');

        $business->addresses()->save($address);

        return redirect('businesses/' . $business->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business, Address $address)
    {
        return view('addresses.edit', compact('business', 'address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business, Address $address)
    {
        $this->validate($request, [
            'street' => 'required'
        ]);

        $address->country = request('country');
        $address->street = request('street');
        $address->phone1 = request('phone1');
        $address->phone2 = request('phone2');
        $address->phone3 = request('phone3');
        $address->email1 = request('email1');
        $address->email2 = request('email2');
        $address->save();

        return redirect('businesses/' . $business->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
