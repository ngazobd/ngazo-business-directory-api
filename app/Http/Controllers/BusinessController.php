<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business;
use App\SubCategory;
use Auth;
use Illuminate\Support\Facades\Bus;

class BusinessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = Business::latest('created_at')->get();
        $subCategories = SubCategory::all();
        return view('businesses.index', compact('businesses', 'subCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sub_categories = SubCategory::orderBy('name')->get();
        return view('businesses.create', compact('sub_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'brand_name' => 'required'
        ]);

        $business = new Business();
        $business->brand_name = request('brand_name');
        $business->description = request('description');
        $user->business()->save($business);

        $sub_category = SubCategory::find(request('sub_category_id'));
        $sub_category->businesses()->attach($business);

        return redirect('/businesses');
    }

    /**
     * Display the specified resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($id)
    {
        $business = Business::with(['comments', 'likes', 'ratings'])->find($id);
        return view('businesses.show', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business)
    {
        $sub_categories = SubCategory::orderBy('name')->get();
        return view('businesses.edit', compact('business', 'sub_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business)
    {
        $user = Auth::user();

        $this->validate($request, [
            'brand_name' => 'required'
        ]);

        $business->brand_name = request('brand_name');
        $business->description = request('description');
        $user->business()->save($business);

        $sub_category = SubCategory::find(request('sub_category_id'));
        $sub_category->businesses()->attach($business);

        return redirect('/businesses/' . $business->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
