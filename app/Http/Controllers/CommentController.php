<?php

namespace App\Http\Controllers;

use App\Business;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Business $business, Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);

        $comment = new Comment();
        $comment->user_id = auth()->id();
        $comment->body = request('body');

        $business->comments()->save($comment);

        return back();
    }
}
