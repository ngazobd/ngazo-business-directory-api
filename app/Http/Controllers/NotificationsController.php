<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function destroy(User $user)
    {
    	$user->unreadNotifications->map(function ($n) {
    		$n->markAsRead();
    	});

    	return back();
    }
}
