<?php

namespace App\Listeners;

use App\User;
use App\Notifications\BusinessPublished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\BusinessPublished as BusinessWasPublished;

class NotifyAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BusinessPublished  $event
     * @return void
     */
    public function handle(BusinessWasPublished $event)
    {
        $user = User::first();
        $user->notify(new BusinessPublished($event->business));
    }
}
