<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function businesses()
    {
        return $this->belongsToMany(Business::class);
    }
}
