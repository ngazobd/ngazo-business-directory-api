<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable implements CanResetPassword
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getActivity()
    {
        return $this->activity()->with(['user', 'subject'])->latest()->get();
    }

    public function activity()
    {
        return $this->hasMany(Activity::class);
    }

    public function recordActivity($name, $related)
    {
        if (!method_exists($related, 'recordActivity')) {
            throw new Exception("Error Processing Request", 1);

        }

        return $related->recordActivity($name);
    }

    public function business()
    {
        return $this->hasOne(Business::class);
    }
}
