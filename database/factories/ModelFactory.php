<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'avatar' => $faker->imageUrl(100, 100),
        'gender' => 'Male',
        'account_type' => $faker->randomNumber(1),
        'device' => $faker->userAgent,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ]; 
});
$factory->define(App\Business::class, function (Faker\Generator $faker) {

    return [
        'brand_name' => $faker->company,
        'website' => $faker->companyEmail,
        'description' => $faker->paragraph,
        'logo' => 'http://placehold.it/70x70',
        'cover_photo' => 'http://placehold.it/300x300',
        'tin' => $faker->randomNumber(9),
        'user_id' => function () {
            return factory('App\User')->create()->id;
        }
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {

    return [
        'name' => ucfirst($faker->word),
        'image' => 'http://placehold.it/300x300',
        'description' => $faker->paragraph,
        'color' => $faker->hexColor
    ];
});

$factory->define(App\SubCategory::class, function (Faker\Generator $faker) {

    return [
        'name' => ucfirst($faker->word),
        'category_id' => function () {
            return factory('App\Category')->create()->id;
        },
        'image' => 'http://placehold.it/30x300',
        'color' => $faker->hexColor
    ];
});

$factory->define(App\Address::class, function (Faker\Generator $faker) {

    return [
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'country' => $faker->country,
        'region' => $faker->city,
        'district' => $faker->city,
        'street' => $faker->streetName,
        'branch' => $faker->city,
        'email1' => $faker->companyEmail,
        'email2' => $faker->companyEmail,
        'phone1' => $faker->phoneNumber,
        'phone2' => $faker->phoneNumber,
        'phone3' => $faker->phoneNumber,
        'business_id' => function () {
            return factory('App\Business')->create()->id;
        },
    ];
});

$factory->define(App\Gallery::class, function (Faker\Generator $faker) {

    return [
        'business_id' => function () {
            return factory('App\Business')->create()->id;
        },
        'image' => 'http://lorempixel.com/400/200/business',
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'product_price' => $faker->randomNumber(4),
    ];
});

$factory->define(App\Comment::class, function (Faker\Generator $faker) {

    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'business_id' => function () {
            return factory('App\Business')->create()->id;
        },
        'body' => $faker->paragraph,
    ];
});

$factory->define(App\BusinessLike::class, function () {
    return [
        'business_id' => function () {
            return factory('App\Business')->create()->id;
        },
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
    ];
});

$factory->define(App\BusinessRating::class, function (Faker\Generator $faker) {
    return [
        'business_id' => function () {
            return factory('App\Business')->create()->id;
        },
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'value' => $faker->randomDigit(5),
    ];
});

$factory->define(App\BusinessSubCategory::class, function (Faker\Generator $faker) {
    return [
        'business_id' => function () {
            return factory('App\Business')->create()->id;
        },
        'sub_category_id' => function () {
            return factory('App\SubCategory')->create()->id;
        },
    ];
});
