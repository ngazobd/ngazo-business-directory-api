<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Create Category
    	$category = factory('App\Category')->create();
    	// Create SubCategory
    	$subCategory = factory('App\SubCategory')->create(['category_id' => $category->id]);
    	// User register him/herself
        $user = factory('App\User')->create();
        // User creates a business
        $business = factory('App\Business')->create(['user_id' => $user->id]);
        // User add that business to a existing sub category
        factory('App\BusinessSubCategory')->create(['business_id' => $business->id, 'sub_category_id' => $subCategory->id]);
        // User adds address for the business
        factory('App\Address')->create(['business_id' => $business->id]);
        // User add image to his/her business
        factory('App\Gallery')->create(['business_id' => $business->id]);
        // User comment in his/her business
        factory('App\Comment')->create(['business_id' => $business->id, 'user_id' => $user->id]);
        // User likes his/her business
        factory('App\BusinessLike')->create(['business_id' => $business->id, 'user_id' => $user->id]);
        // User rate his/her business
        factory('App\BusinessRating')->create(['business_id' => $business->id, 'user_id' => $user->id]);
    }
}
