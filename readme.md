# Ngazo Business Directory API

This app can be accessed here [http://fathomless-everglades-35350.herokuapp.com](http://fathomless-everglades-35350.herokuapp.com)

## API Reference

[http://fathomless-everglades-35350.herokuapp.com/api/v1](http://fathomless-everglades-35350.herokuapp.com/api/v1)


## API Tests

### Feature

#### Users

- [x] Can browse all users
- [x] Can browse a specific user
- [x] Can create a user
- [x] Can update a user
- [x] Can delete a user

#### Businesses

- [x] Can browse all businesses
- [x] Can browse a specific business
- [x] Can create a business
- [x] Can update a business
- [x] Can delete a business

#### User Actions

- [x] User can like a business
- [x] User can unlike a business
- [x] User can rate a business
