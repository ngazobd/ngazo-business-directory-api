@component('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Address</h3>
                    </div>
                    <div class="panel-body">

                        <form action="/businesses/{{ $business->id }}/addresses/{{ $address->id }}" method="POST"
                              role="form">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group">
                                <label for="street">Street</label>
                                <input type="text" class="form-control" name="street" id="street"
                                       placeholder="Address" required="required">
                            </div>

                            <div class="form-group">
                                <label for="phone1">Primary phone</label>
                                <input type="text" class="form-control" name="phone1" id="phone1"
                                       placeholder="Primary phone" required="required">
                            </div>

                            <div class="form-group">
                                <label for="phone2">Secondary phone</label>
                                <input type="text" class="form-control" name="phone2" id="phone2"
                                       placeholder="Secondary phone" required="required">
                            </div>

                            <div class="form-group">
                                <label for="phone3">Tertiary phone</label>
                                <input type="text" class="form-control" name="phone3" id="phone3"
                                       placeholder="Tertiary phone" required="required">
                            </div>

                            <div class="form-group">
                                <label for="email1">Primary email</label>
                                <input type="email" class="form-control" name="email1" id="brand_name"
                                       placeholder="Secondary email" required="required">
                            </div>

                            <div class="form-group">
                                <label for="email2">Secondary email</label>
                                <input type="email" class="form-control" name="email2" id="brand_name"
                                       placeholder="Secondary email" required="required">
                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@endcomponent
