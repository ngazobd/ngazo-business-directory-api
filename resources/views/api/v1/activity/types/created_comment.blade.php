<div class="stream-small">
    <span class="label label-success"> Comment</span>
    <span class="text-muted"> {{  $event->created_at->diffForHumans() }} </span> / <a
            href="/dashboard/users/{{ $event->user->id }}">{{ $event->user->first_name }} {{ $event->user->last_name }}</a>
    Commented on <a href="/businesses/{{ $event->subject_id }}">{{ $event->subject->brand_name }}</a> business.
</div>