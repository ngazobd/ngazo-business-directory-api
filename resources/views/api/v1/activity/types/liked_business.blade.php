<div class="stream-small">
    <span class="label label-success"> Like</span>
    <span class="text-muted"> {{  $event->created_at->diffForHumans() }} </span> / <a
            href="/dashboard/users/{{ $event->user->id }}">{{ $event->user->first_name }} {{ $event->user->last_name }}</a> liked
    <a href="/businesses/{{ $event->subject_id }}">{{ $event->subject->brand_name }}</a>
</div>