<div class="stream-small">
    <span class="label label-default"> Update</span>
    <span class="text-muted"> {{  $event->created_at->diffForHumans() }} </span> / <a href="/dashboard/users/{{ $event->user->id }}">{{ $event->user->first_name }} {{ $event->user->last_name }}</a>
    updated <a href="/businesses/{{ $event->subject_id }}">{{ $event->subject->brand_name }}</a>
</div>