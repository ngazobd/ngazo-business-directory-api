@extends('layouts.app')

@section('content')
    <div class="container home">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">API key</h3>
                    </div>
                    <div class="panel-body">

                        <form action="{{ url(Auth::user()->username.'/api-token') }}" method="POST"
                              class="form-horizontal" role="form">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="api_token" class="col-sm-2 control-label">API Token</label>
                                <div class="col-sm-10">
                                    @if(Auth::user()->api_token)
                                        <code>{{ Auth::user()->api_token }}</code>
                                    @else
                                        Click generate to generate your API Token
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary">Generate</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
