@extends('layouts.app')

@section('content')
    <div class="container home">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">You</h3>
                    </div>
                    <div class="panel-body">

                        <form action="" method="POST" class="form-horizontal" role="form">

                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-5">
                                    <input type="text" name="username" id="username"
                                           value="{{ Auth::user()->username }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="first_name" class="col-sm-2 control-label">First name</label>
                                <div class="col-sm-5">
                                    <input type="text" name="first_name" id="first_name"
                                           value="{{ Auth::user()->first_name }}" class="form-control">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="first_name" class="col-sm-2 control-label">Last name</label>
                                <div class="col-sm-5">
                                    <input type="text" name="first_name" id="first_name"
                                           value="{{ Auth::user()->first_name }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Last name</label>
                                <div class="col-sm-5">
                                    <input type="email" name="email" id="email" value="{{ Auth::user()->email }}"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                                <div class="col-sm-5">
                                    <input type="text" name="mobile" id="mobile" value="{{ Auth::user()->mobile }}"
                                           class="form-control">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
