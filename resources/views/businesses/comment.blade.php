<div class="media">
    <a class="pull-left" href="#">
        <img class="media-object" src="{{ $comment->user->avatar }}" alt="">
    </a>
    <div class="media-body">
        <h4 class="media-heading">
            {{ $comment->user->name }} said 
            <small>
                {{ $comment->created_at->diffForHumans() }}
            </small>
        </h4>
        <blockquote>
            {{ $comment->body }}
        </blockquote>
    </div>
</div>