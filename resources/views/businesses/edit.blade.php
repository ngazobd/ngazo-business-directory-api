@component('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit {{ $business->brand_name }}</h3>
                    </div>
                    <div class="panel-body">

                        <form action="/businesses" method="POST" role="form">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="brand_name">Title</label>
                                <input type="text"
                                       class="form-control"
                                       name="brand_name"
                                       id="brand_name"
                                       value="{{ $business->brand_name }}"
                                       placeholder="Your business name" required="required">
                            </div>

                            <div class="form-group">
                                <label for="sub_category_id">Category</label>
                                <select name="sub_category_id" id="sub_category_id" class="form-control"
                                        required="required">
                                    <option value="">-Select-</option>
                                    @foreach($sub_categories as $sub_category)
                                        <option value="{{ $sub_category->id }}">{{ $sub_category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description"
                                          id="description"
                                          class="form-control"
                                          rows="5"
                                          required="required">{{ $business->description }}</textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@endcomponent
