@component('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if($businesses->count())
                    @foreach($businesses as $business)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a href="{{ $business->path() }}">
                                        {{ $business->brand_name }}
                                    </a>
                                    <small>{{ $business->created_at->diffForHumans() }}</small>
                                </h3>
                            </div>
                            <div class="panel-body">
                                {{ $business->description }}
                            </div>
                            <div class="panel-footer">
                                @if($business->tags)
                                    @foreach($business->tags as $tag)
                                        <span class="label label-info">{{ $tag->name }}</span>
                                    @endforeach
                                @endif
                                <span>
							{{ $business->likes()->count() }} <i class="fa fa-thumbs-up"></i>
						</span>
                                <span>
							{{ $business->ratings()->count() }} <i class="fa fa-star"></i>
						</span>
                                <span>
							{{ $business->comments()->count() }} <i class="fa fa-comments"></i>
						</span>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="alert alert-info text-center">
                                No Business
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if ($subCategories->count())
                            @foreach ($subCategories as $subCategory)
                                <span class="badge">
                                    {{ $subCategory->name }}
                                </span>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@endcomponent
