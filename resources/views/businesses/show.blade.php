@component('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $business->brand_name }}</h3>
                    </div>
                    <div class="panel-body">
                        {{ $business->description }}
                    </div>
                    <div class="panel-footer">
                        @if($business->tags)
                            @foreach($business->tags as $tag)
                                <span class="label label-info">{{ $tag->name }}</span>
                            @endforeach
                        @endif
                        <span>
							{{ $business->likes()->count() }} <i class="fa fa-thumbs-up"></i>
						</span>
                        <span>
							{{ $business->ratings()->count() }} <i class="fa fa-star"></i>
						</span>
                        <span>
							{{ $business->comments()->count() }} <i class="fa fa-comments"></i>
						</span>
                    </div>
                </div>
  <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Address
                        </h3>
                    </div>
                    <div class="panel-body">

                                @if($business->addresses)
                                    @foreach($business->addresses->chunk(2) as $addressSet)
                                        <div class="row">
                                            @foreach ($addressSet as $address)
                                                <div class="col-xs-6">
                                                    <h3>{{ $address->branch }}</h3>
                                                    <p>
                                                        <i class="fa fa-phone"></i> {{ $address->phone1 }}
                                                        <br>
                                                        <i class="fa fa-phone"></i> {{ $address->phone2 }}
                                                    </p>
                                                    <p>
                                                        <i class="fa fa-envelope"></i> {{ $address->email1 }}
                                                        <br>
                                                        <i class="fa fa-envelope"></i> {{ $address->email2 }}
                                                    </p>
                                                    <p>
                                                        <i class="fa fa-map-marker"></i> {{ $address->street }} ({{ $address->lat }}, {{ $address->lng }})
                                                    </p>            
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif                      
                    </div>
                    @if(Auth::check())
                        @if($business->user->id == Auth::user()->id)
                            <div class="panel-footer">
                                <a href="/businesses/{{ $business->id }}/addresses/1/edit"><i class="fa fa-edit"></i>
                                    Edit</a>
                            </div>
                        @endif
                    @endif
                </div>                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Reviews
                        </h3>
                    </div>
                    <div class="panel-body">
                        @if($business->comments()->count())
                            @foreach($business->comments as $comment)
                                @include('businesses.comment')
                            @endforeach
                        @else
                            Be the first to leave a review
                        @endif

                        @if(Auth::check())

                            <form method="POST" action="/businesses/{{ $business->id }}/comments">
                                {{ csrf_field() }}

                                <div class="form-group">
                                <textarea name="body" class="form-control"
                                          placeholder="Leave your review"></textarea>
                                </div>
                                <button class="btn btn-primary pull-right">Submit</button>

                            </form>

                        @else

                            <div class="text-center">
                                Please <a href="/login">Sign In</a> to leave your review
                            </div>

                        @endif

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Gallery</h3>
                    </div>
                    <div class="panel-body">
                        @if ($business->galleries->count())
                            @foreach ($business->galleries->chunk(2) as $galleriesSet)
                                <div class="row">
                                    @foreach ($galleriesSet as $gallery)
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <a href="#" class="thumbnail">
                                                <img src="{{ $gallery->image }}" alt="">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @else
                            <div class="text-center">
                                <i class="fa fa-star fa-5x"></i>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@endcomponent
