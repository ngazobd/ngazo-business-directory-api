@extends('layouts.app')

@section('content')
    <div class="container-fluid home">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">You</h3>
                    </div>
                    <div class="panel-body">

                       <h1>Welcome </h1> {{ Auth::user()->first_name }}

                    </div>
                </div>

            </div>
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notifications <span class="badge">{{ Auth::user()->unreadNotifications->count() }}</span></h3>
                    </div>
                    <div class="panel-body">
                    @if(Auth::user()->unreadNotifications->count())
                        @foreach(Auth::user()->unreadNotifications as $notification)
                            @include('notifications.' . snake_case(class_basename($notification->type)))
                        @endforeach
                    @endif
                    </div>
                    @if(Auth::user()->unreadNotifications->count())
                        <div class="panel-footer">
                            <form method="POST" action="/users/{{ Auth::id() }}/notifications">
                                {{ csrf_field() }}
                                {{ method_field('DELETe') }}
                                <button type="submit" class="btn btn-primary btn-sm">Mark All As Read</button>
                            </form>                    
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
