<div class="list-group">
    <a href="{{ url('home') }}" class="list-group-item active"><i class="fa fa-home fa-fw"></i> Home</a>
    <a href="{{ url('docs') }}" class="list-group-item"><i class="fa fa-book fa-fw"></i> Docs</a>
    <a href="{{ url(Auth::user()->username.'/api-token') }}" class="list-group-item"><i class="fa fa-key fa-fw`"></i>
        API Token</a>
    <a href="{{ url(Auth::user()->username.'/profile') }}" class="list-group-item"><i class="fa fa-user fa-fw"></i>
        Profile</a>
</div>