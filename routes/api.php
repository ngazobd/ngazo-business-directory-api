<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'API\v1', 'prefix' => 'v1'], function () {
    Route::get('/', 'ApiController@index');
    Route::resource('businesses', 'BusinessesController');
    Route::post('businesses/{business}/comments', 'CommentsController@store');
    Route::resource('categories', 'CategoriesController');
    Route::resource('sub-categories', 'SubCategoriesController');
    Route::resource('addresses', 'AddressesController');
    Route::post('businesses/{business}/comments', 'CommentController@store');
    Route::post('businesses/{business}/likes', 'LikesController@store');
    Route::delete('businesses/{business}/likes', 'LikesController@destroy');
    Route::post('businesses/{business}/ratings', 'RatingsController@store');
    Route::resource('users', 'UsersController');
    Route::get('users/{user}/notifications', 'NotificationsController@index');
    Route::delete('users/{user}/notifications', 'NotificationsController@destroy');
    Route::resource('addresses', 'AddressesController');
    Route::resource('comments', 'CommentController');
});
