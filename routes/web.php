<?php

use Illuminate\Support\Facades\Auth;
use App\Notifications\BusinessPublished;

Route::get('/', function () {
    $businesses = App\Business::latest()->get();
    $subCategories = App\SubCategory::all();
    return view('businesses.index', compact('businesses', 'subCategories'));
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/{username}/profile', function () {
        return view('api.v1.users.edit');
    });
    Route::get('/{username}/api-token', function () {
        return view('api.v1.users.api-token');
    });
    Route::post('/{username}/api-token', function () {
        $user = Auth::user();
        $user->api_token = str_random(60);
        $user->save();

        return back();
    });
});

Route::group(['namespace' => 'API\v1'], function () {
    Route::get('docs', 'PagesController@docs');
    Route::get('dashboard/users/{username}/activity', 'ActivitiesController@show');
});

Route::get('dashboard', function () {

    return view('dashboard');
})->middleware('auth');

Route::get('businesses/{id}/likes', function ($id) {
    $business = App\Business::findOrFail($id);

    $user = Auth::user();

    $business->likes()->attach($user);

    $user->recordActivity('liked', $business);
});

Route::group(['namespace' => 'Dashboard', 'middleware' => 'auth'], function () {
    Route::resource('dashboard/users', 'UsersController');
    Route::resource('dashboard/categories', 'CategoriesController');
});

Route::get('businesses', 'BusinessController@index');
Route::get('businesses/{business}', 'BusinessController@show');

Route::post('businesses/{business}/comments', 'CommentController@store');
Route::resource('businesses.addresses', 'AddressController');

Route::delete('users/{user}/notifications', 'NotificationsController@destroy');
