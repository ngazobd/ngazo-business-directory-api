<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BusinessesTest extends TestCase
{
	use DatabaseMigrations;

	public function setUp()
	{
		parent::setUp();

		$this->business = factory('App\Business')->create();
	}

	/**
	 * Test that a user can view all businesses
	 *
	 * @return void
	 */
	/** @test */
    public function a_user_can_view_all_businesses()
    {
        $response = $this->get('/api/v1/businesses');
        $response->assertSee($this->business->brand_name)
        		 ->assertStatus(200);
    }

    /** @test */
    public function a_user_can_view_a_single_business()
    {
    	$response = $this->get('/api/v1/businesses/' . $this->business->id);
    	$response->assertSee($this->business->brand_name)
    		     ->assertStatus(200);
    }

    /** @test */
    public function a_user_can_read_comments_that_are_associated_with_a_business()
    {
    	$comment = factory('App\Comment')
    				->create(['business_id' => $this->business->id]);

    	$response = $this->get('/api/v1/businesses/' . $this->business->id);
    	$response->assertSee($comment->body);
    }

    /** @test */
    public function a_user_can_create_a_business()
    {
        $business = factory('App\Business')->make();

        $response = $this->post('/api/v1/businesses', $business->toArray());

        $response->assertStatus(200)
                 ->assertJson([
                    'created' => true,
                ]); 
    }

    /** @test */
    public function a_user_can_update_a_business()
    {
        $business = factory('App\Business')->create();

        $newBusiness = factory('App\Business')->make();

        $response = $this->patch('/api/v1/businesses/' . $this->business->id, $newBusiness->toArray());

        $response->assertStatus(200)
                 ->assertJson([
                    'updated' => true,
                ]);        
    }

    /** @test */
    public function a_user_can_delete_a_business()
    {
        $response = $this->delete('/api/v1/businesses/' . $this->business->id);

        $response->assertStatus(200)
                 ->assertJson([
                    'deleted' => true,
                ]);
    }
}
