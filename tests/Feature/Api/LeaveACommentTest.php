<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LeaveACommentTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */
    public function an_authenticated_user_may_leave_comments_in_businesses()
    {
        $user = factory('App\User')->create();
        $this->be($user);

        $business = factory('App\Business')->create();

        $comment = factory('App\Comment')->make();

        $response = $this->post('/businesses/' . $business->id . '/comments', $comment->toArray());
        
        $this->get('/businesses/' . $business->id)
        	 ->assertSee($comment->body);
    }
}
