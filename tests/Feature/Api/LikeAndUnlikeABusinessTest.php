<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LikeAndUnlikeABusinessTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_like_a_business()
    {
        $user = factory('App\User')->create();

        $business = factory('App\Business')->create();

        $response = $this->post('/api/v1/businesses/' . $business->id . '/likes', ['user_id' => $user->id]);

        $response->assertStatus(200)
            ->assertJson([
                'liked' => true,
            ]);
    }

    /** @test */
    public function a_user_can_unlike_a_business()
    {
        $user = factory('App\User')->create();

        $business = factory('App\Business')->create();

        $response = $this->delete('/api/v1/businesses/' . $business->id . '/likes', ['user_id' => $user->id]);

        $response->assertStatus(200)
            ->assertJson([
                'unliked' => true,
            ]);
    }
}
