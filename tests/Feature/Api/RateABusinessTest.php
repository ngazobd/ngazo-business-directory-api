<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RateABusinessTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_rate_a_business()
    {
        $user = factory('App\User')->create();

        $business = factory('App\Business')->create();

        $response = $this->post('/api/v1/businesses/' . $business->id . '/ratings', ['user_id' => $user->id, 'value' => 5]);

        $response->assertStatus(200)->assertJson(['rated' => true]);
    }
}
