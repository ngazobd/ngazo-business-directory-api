<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
	use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_user_can_browse_users()
    {
    	$user = factory('App\User')->create();
    	$response = $this->get('/api/v1/users');
    	$response->assertSee($user->name)
    			 ->assertStatus(200);
    }

    public function test_a_user_can_browse_a_single_user()
    {
    	$user = factory('App\User')->create();
    	$response = $this->get('/api/v1/users/' . $user->id);
    	$response->assertSee($user->name)
    			 ->assertStatus(200);
    }

    public function test_a_user_can_be_created()
    {
        $user = factory('App\User')->make();

        $response = $this->post('/api/v1/users', $user->toArray());

       $response->assertStatus(200)
                ->assertJson([
                    'created' => true,
                ]);
    }

    public function test_a_user_can_be_updated()
    {
        $user = factory('App\User')->create();

        $newUser = factory('App\User')->make();

        $response = $this->patch('/api/v1/users/' . $user->id, $newUser->toArray());

        $response->assertStatus(200)
                 ->assertJson([
                    'updated' => true,
                ]);
    }

    public function test_a_user_can_be_deleted()
    {
        $user = factory('App\User')->create();
        
        $response = $this->delete('/api/v1/users/' . $user->id); 

        $response->assertStatus(200)
                 ->assertJson([
                    'deleted' => true,
                ]);        
    }
}
