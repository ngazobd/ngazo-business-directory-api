<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BusinessesTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->business = factory('App\Business')->create();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_user_can_browse_businesses()
    {
        $response = $this->get('/businesses');
        $response->assertSee($this->business->brand_name);
    }

    public function test_a_user_can_view_a_single_business()
    {
        $response = $this->get('/businesses/' . $this->business->id);
        $response->assertSee($this->business->brand_name);        
    }

    public function test_a_user_can_read_reviews_that_are_associated_with_a_business()
    {
        $comment = factory('App\Comment')->create(['business_id' => $this->business->id]);

        $response = $this->get('/businesses/' . $this->business->id);
        $response->assertSee($comment->body);
    }
}
