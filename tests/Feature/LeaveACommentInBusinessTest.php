<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LeaveACommentInBusinessTest extends TestCase
{
	use DatabaseMigrations;

    public function test_an_authenticated_user_may_leave_a_comment_in_business()
    {
    	$user = factory('App\User')->create();
    	$this->be($user);

    	$business = factory('App\Business')->create();

    	$comment = factory('App\Comment')->make();

    	$response = $this->post('/businesses/' . $business->id . '/comments', $comment->toArray());

    	$response = $this->get($business->path());

    	$response->assertSee($comment->body); 
    }
}
