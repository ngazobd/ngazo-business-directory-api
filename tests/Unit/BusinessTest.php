<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BusinessTest extends TestCase
{
    use DatabaseMigrations;

    public function test_a_business_has_comments()
    {
    	$business = factory('App\Business')->create();

    	$this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $business->comments);
    }

    public function test_a_business_has_a_creator()
    {
    	$business = factory('App\Business')->create();

    	$this->assertInstanceOf('App\User', $business->user);
    }
}
