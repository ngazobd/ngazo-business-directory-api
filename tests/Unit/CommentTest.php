<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentTest extends TestCase
{
	use DatabaseMigrations;
	
    public function test_it_has_a_user()
    {
    	$comment = factory('App\Comment')->create();

    	$this->assertInstanceOf('App\User', $comment->user);
    }
}
